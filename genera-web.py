#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import re
import shutil
import os

any = 2025

def calcula_total_setmana(s):
    buit = True
    ret = 0
    for x in s.values():
        if x != '' and x != '-':
            buit = False
            ret += int(x)
    return str(ret) if not buit else ''


def calcula_total_tot(s):
    buit = True
    ret = 0
    for i in range(1, 8):
        if s[i] != '':
            buit = False
            ret += int(s[i])
    return str(ret) if not buit else ''


def calcula_totals_equip(e):
    e['p'] = {}
    for i in range(1, 8):
        e['p'][i] = calcula_total_setmana(e['puntuacions']['l' + str(i)])

    e['p']['total'] = calcula_total_tot(e['p'])


def clau_sort_puntuacio(e):
    if e['p']['total'] == '':
        return str(9999) + e['id']
    else:
        return str(999 - int(e['p']['total'])).zfill(4) + e['id']


def clau_sort_alfabetic(e):
    return e['id']


def termini(i):
    #actualitzar a mesura que es vagin tancant les llistes
    if (i<=0):
        return 'termini tancat'
    else:
        if i == 1:
            return 'termini: 27 de febrer a les 21:00h'
        elif i == 2:
            return 'termini: 27 de febrer a les 21:00h'
        elif i == 3:
            return 'termini: 1 de març a les 12:00'
        elif i == 4:
            return 'termini: 3 de març a les 12:00'
        elif i == 5:
            return 'termini: 4 de març a les 12:00'
        elif i == 6:
            return 'termini 5 de març a les 12:00'
        elif i == 7:
            return 'termini: 6 de març a les 12:00'


def resolucions(i):
    ret = ''
    if (i<=7):
        for p in res[i]:
            if ret == '':
                ret += ', resolucions: '
            else:
                ret += ', '
            ret += '<a href="resolucions/' + str(any) + '/llista-' + str(i) + '/p' + p[0] + '-' + p[1] + p[2] + '">P' + p[1] + '</a>'
    return ret


def crea_links_llistes():
    llistes = ''    # Llistes de problemes
    for i in range(1, 8):
        if ll[i]:
            if (i != 1):
                llistes += '\n        '
            llistes += '<li><a href="llistes/' + str(any) + '/llista-' + str(i) + '.pdf">Llista ' + str(i) + '</a> (' + termini(i) + ')' + resolucions(i) + '</li>'

    return llistes


def calcula_dades_equips():
    for e in d['equips']:
        e['id'] = e['nom'].lower().replace(' ', '-').replace('\'', '-')
        calcula_totals_equip(e)


def trofeu(n):
    color = ''
    if (n == 0):
        color = '#ffd700'
    elif (n == 1):
        color = '#c0c0c0'
    elif (n == 2):
        color = '#cd7f32'

    if color != '':
        return '<svg style="width:17px;fill:' + color + '" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M552 64H448V24c0-13.3-10.7-24-24-24H152c-13.3 0-24 10.7-24 24v40H24C10.7 64 0 74.7 0 88v56c0 35.7 22.5 72.4 61.9 100.7 31.5 22.7 69.8 37.1 110 41.7C203.3 338.5 240 360 240 360v72h-48c-35.3 0-64 20.7-64 56v12c0 6.6 5.4 12 12 12h296c6.6 0 12-5.4 12-12v-12c0-35.3-28.7-56-64-56h-48v-72s36.7-21.5 68.1-73.6c40.3-4.6 78.6-19 110-41.7 39.3-28.3 61.9-65 61.9-100.7V88c0-13.3-10.7-24-24-24zM99.3 192.8C74.9 175.2 64 155.6 64 144v-16h64.2c1 32.6 5.8 61.2 12.8 86.2-15.1-5.2-29.2-12.4-41.7-21.4zM512 144c0 16.1-17.7 36.1-35.3 48.8-12.5 9-26.7 16.2-41.8 21.4 7-25 11.8-53.6 12.8-86.2H512v16z"/></svg> '
    # elif (n == 3):
    #     return '<svg style="width:25px;margin-right:0.2em;vertical-align:-0.2em"  xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" preserveAspectRatio="xMidYMid meet" viewBox="0 0 64 64"><path fill="#3e4347" d="m48.5 34.1l4.7-2.8c1.6-1.2 4.4 0 6.2 2.7c1.7 2.7 1.8 5.8.2 7c-.1.1-.2.2-.4.2l-4.4 2.6l-6.3-9.7"/><path fill="#62696d" d="M49.1 41c1.7 2.7 4.1 4 5.8 2.8c1.6-1.2 1.6-4.3-.2-7c-1.7-2.7-4.5-3.9-6.2-2.7c-1.5 1.1-.7 5 .6 6.9"/><path fill="#3e4347" d="M50.4 40.1c.9 1.4 2.1 2.1 3 1.4c.9-.6.8-2.3-.1-3.7c-.9-1.4-2.4-2.1-3.3-1.4c-.8.6-.5 2.3.4 3.7"/><path fill="#b2c1c0" d="M51.2 38c.9-.5 2.1.8 1.5 1.7l-3.9 2.4l-1.2-2.1l3.6-2"/><path fill="#9c2c1b" d="M47.7 33.4v5.4L43.6 35z"/><path fill="#3e4347" d="m28.5 16.6l5.4-3.2c1.9-1.4 5.1 0 7.1 3.1s2.1 6.7.2 8.1c-.1.1-.3.2-.4.2l-5 3l-7.3-11.2"/><path fill="#62696d" d="M29.1 24.5c2 3.1 4.8 4.6 6.7 3.2c1.9-1.4 1.8-5-.2-8.1c-2-3.1-5.2-4.5-7.1-3.1c-1.8 1.3-.8 5.8.6 8"/><path fill="#9c2c1b" d="m51.1 48.9l-11.4-6.3l-2.9 1.7l-11.6-2.6l-14.3-10.8v-5.1l14.3 10L36.8 40l2.9-2.1l11.4 9.2z"/><path fill="#d33b23" d="m55.3 44.4l-4.2 2.7l-11.4-9.2l-2.9 2.1l-11.6-4.2l-14.3-10l3.6-2.1l2.4.6l8.3-4.6l-.1-1.1l3.3-2l14.9 9.9l4.4 6.9l-3.1 1.5z"/><path fill="#f15744" d="M15.6 15.8v4.4l10.6 5.1l4.7-2.2l-12.4-4.8v-2.5z"/><path fill="#9c2c1b" d="M30.9 26.9v-3.8l-4.7 2.2l-10.6-5.1v-4.4H15v4.8L26.2 29z"/><path fill="#f15744" d="m43.3 26.5l4.4 6.9L36.8 40l-11.6-4.2z"/><g fill="#ffe62e"><path d="m43.3 26.5l4.4 6.9l-1.2.7l-4.5-7z"/><path d="m28.5 16.6l14.8 9.9l-1.3.6l-14.7-9.8zm-1 18L38.4 39l-1.6 1l-11.6-4.2z"/><path d="m12.7 24.7l14.8 9.9l-2.3 1.2l-14.3-10z"/></g><path fill="#9c2c1b" d="M26.2 29c-.3 2.1 2.9 4.8 6 5.2c5.4.8 7.9-2.3 6.3-5.5c-1.5-2.9-7.5-2.3-7.5-2.3L26.2 29"/><path fill="#3e4347" d="m5 29.9l6.2-3.7c2.2-1.6 5.8 0 8 3.5c2.3 3.5 2.4 7.6.2 9.1c-.1.1-.3.2-.5.3l-5.7 3.4L5 29.9"/><path fill="#62696d" d="M5.7 38.9c2.3 3.5 5.4 5.2 7.6 3.6c2.2-1.6 2-5.6-.2-9.1c-2.3-3.5-5.9-5.1-8-3.5c-2.1 1.5-1 6.5.6 9"/><path fill="#3e4347" d="M7.5 37.7c1.2 1.9 2.7 2.7 3.9 1.9c1.1-.8 1.1-3-.1-4.9c-1.3-1.8-3.2-2.7-4.4-1.8c-1.1.8-.6 2.9.6 4.8"/><path fill="#b2c1c0" d="M43.5 42.5c.9-.5 2.1.8 1.5 1.7l-3.9 2.4l-1.2-2.1l3.6-2"/><path fill="#3e4347" d="m31.5 43.3l5.4-3.2c1.9-1.4 5.1 0 7.1 3.1s2.1 6.7.2 8.1c-.1.1-.3.2-.4.2l-5 3l-7.3-11.2"/><path fill="#62696d" d="M32.1 51.3c2 3.1 4.8 4.6 6.7 3.2c1.9-1.4 1.8-5-.2-8.1c-2-3.1-5.2-4.5-7.1-3.1c-1.8 1.3-.8 5.8.6 8"/><path fill="#3e4347" d="M33.7 50.2c1.1 1.6 2.4 2.4 3.4 1.7s1-2.7-.1-4.3c-1.1-1.6-2.8-2.4-3.8-1.6c-1 .7-.6 2.6.5 4.2"/><path fill="#d33b23" d="m22.7 9l4 3.6l-18.6 9.8L2 16.8z"/><path fill="#42ade2" d="M27.6 28.7c0 3 3.6 4.5 6.4 4.5c2.9 0 3.9-1.5 3.9-4.5s-2.3-5.4-5.2-5.4c-2.8 0-5.1 2.4-5.1 5.4"/><path fill="#3e4347" d="M30.7 28.6c.1-1.1 3.9-1.1 6.8-1.3c.9-.1.8 3.3 0 3.4c-4.6.4-6.9-1-6.8-2.1"/><path fill="#f15744" d="m60 40.9l2 4.4l-13.1 8.8l-5.9-2.7l11.8-7.3z"/><path fill="#ffe62e" d="m4.8 15.7l6.4 5.1l1.8-.9l-5.8-5.1zm14.4-5.4l4.3 4l1.3-.7l-4-3.9zm38 32.4l2.2 4.3l1.1-.7l-2-4.4zm-11.6 7.1l4.9 3.2l1.1-.8L47 49z"/></svg>'
    else:
        return ''

def crea_files_puntuacions():
    d['equips'].sort(key=clau_sort_puntuacio)
    fp = ''         # Files de la taula de puntuacions
    primer = True
    for i, e in enumerate(d['equips']):
        if primer:
            primer = False
        else:
            fp += '\n          '

        s = trofeu(i)
        s = ''      # comenta aquesta línia per a que surtin els trofeus
        fp += '<tr>\n'
        fp += '            <td>' + s + '<a href="#' + e['id'] + '">' + e['nom'] + '</a></td>\n'
        fp += '            <td><strong>' + e['p']['total'] + '</strong></td>\n'
        for i in range(1, 8):
            fp += '            <td>' + e['p'][i] + '</td>\n'
        fp += '          </tr>'

    return fp


def crea_informacio_equips():
    d['equips'].sort(key=clau_sort_alfabetic)
    primer = True
    ie = ''
    for e in d['equips']:
        if primer:
            primer = False
        else:
            ie += '\n      '
        ie += '<strong id="' + e['id'] + '">' + e['nom'] + '</strong>\n'
        ie += ' <a href="img/' + str(any) + '/equips/grup-' + str(indexs[e['nom']]) + '.pdf">[emblema]</a>\n'
        ie += '      <ul>\n'
        if len(e['integrants']) == 1:
            ie += '        <li>Integrants: ' + e['integrants'][0] + '.</li>\n'
        else:
            ie += '        <li>Integrants: ' + ", ".join(e['integrants'][:-1]) + " i " + e['integrants'][-1] + '.</li>\n'
        ie += '        <li>Puntuacions per problema</li>\n'
        ie += '        <div class="contenidor-taula">\n'
        ie += '          <table>\n'
        ie += '            <tr>\n'
        ie += '              <th></th>\n'
        ie += '              <th>Total</th>\n'
        ie += '              <th>P1</th>\n'
        ie += '              <th>P2</th>\n'
        ie += '              <th>P3</th>\n'
        ie += '              <th>P4</th>\n'
        ie += '              <th>P5</th>\n'
        ie += '              <th>P6</th>\n'
        ie += '              <th>Bonus</th>\n'
        ie += '            </tr>\n'

        for i in range(1, 8):
            ie += '            <tr>\n'
            ie += '              <th>Llista ' + str(i) + '</th>\n'
            ie += '              <td><strong>' + e['p'][i] + '</strong></td>\n'
            for j in range(1, 7):
                ie += '              <td>' + e['puntuacions']['l' + str(i)]['p' + str(j)] + '</td>\n'
            ie += '              <td>' + e['puntuacions']['l' + str(i)]['bonus'] + '</td>\n'
            ie += '            </tr>\n'
        ie += '          </table>\n'
        ie += '        </div>\n'
        ie += '      </ul>'

    return ie



with open('dades.json', 'r') as f:
    d = json.load(f)

with open('index.html', 'r') as f:
    html = f.read()

if os.path.isdir('public'):
    shutil.rmtree('public')
shutil.copytree('edicions-anteriors', 'public')
shutil.copytree('img', 'public/img/' + str(any))

os.makedirs('public/llistes/' + str(any))
os.makedirs('public/resolucions/' + str(any))

# Té llargada més gran per a indexar-ho sobre 1 (com els noms dels PDFs)
# L'he feta encara més gran per a que no falli al fer i+2 (per mirar si el
# termini ha acabat)
ll = [False] * 10
# ll[8] = True      # final termini llista 6
# ll[9] = True      # final termini llista 7

for i in range(1, 8):
    if os.path.isfile('llistes/llista-' + str(i) + '.pdf'):
        shutil.copyfile('llistes/llista-' + str(i) + '.pdf', 'public/llistes/' + str(any) + '/llista-' + str(i) + '.pdf')
        ll[i] = True

# Indexar sobre 1...
res = [[], [], [], [], [], [], [], []]
if os.path.isdir('resolucions'):
    for i in range(1, 8):
        if os.path.isdir('resolucions/llista-' + str(i)):
            primer = True
            for fitxer in os.listdir('resolucions/llista-' + str(i)):
                if primer:
                    primer = False
                    os.mkdir('public/resolucions/' + str(any) + '/llista-' + str(i))
                r = re.search('p([1-6])-([0-9]+)(\.[a-z]+)', fitxer)
                res[i].append([r.group(1), r.group(2), r.group(3)])
                sortida = 'public/resolucions/' + str(any) + '/llista-' + str(i) + '/' + 'p' + r.group(1) + '-' + r.group(2) + r.group(3)
                shutil.copyfile('resolucions/llista-' + str(i) + '/' + fitxer , sortida)
            res[i].sort(key=lambda x : int(x[0]))


calcula_dades_equips()

# Calcula indexs per a les imatges dels equips amb l'ordre del json
indexs = {}
for i, e in enumerate(d['equips']):
    indexs[e['nom']] = i + 1

html = re.sub('<!-- comentari(.|\n)*?/comentari -->', '', html)
html = re.sub('/\* comentari(.|\n)*?/comentari \*/', '', html)
html = re.sub('<!-- llistes -->', crea_links_llistes(), html)
html = re.sub('<!-- puntuacions-equips -->', crea_files_puntuacions(), html)
html = re.sub('<!-- informacio-equips -->', crea_informacio_equips(), html)

with open('public/index.html', 'w') as f:
    f.write(html)

# Formulari meme per revisions
# shutil.copyfile('revisio.html', 'public/revisio.html')
