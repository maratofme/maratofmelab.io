#!/usr/bin/env bash

cd img/equips

for file in *.jpg *.jpeg *.png *.webp; do
    [ -f "$file" ] || continue
    filename=$(basename -- "$file")
    name="${filename%.*}"
    magick "$file" "$name.pdf"
done
