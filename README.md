# Web de la marató de problemes de la FME

Per tal de generar el web en local simplement cal executar el programa
`genera-web.py` i es generarà el web dins la carpeta `public`.

Les dades es generen a partir del fitxer `dades.json` que té la següent forma
(equips és un vector d'objectes de l'estil que hi ha aquí com a exemple):

```json
{
  "equips": [
    {
      "nom": "",
      "integrants": [
        "",
        "",
        ""
      ],
      "puntuacions": {
        "l1": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": "", "bonus": ""},
        "l2": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": "", "bonus": ""},
        "l3": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": "", "bonus": ""},
        "l4": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": "", "bonus": ""},
        "l5": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": "", "bonus": ""},
        "l6": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": "", "bonus": ""},
        "l7": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": "", "bonus": ""}
      }
    }
  ]
}
```

Si els fitxers es posen amb el nom correcte i al directori correcte, el web es
generarà automàticament. L'estructura de fitxers ha de quedar així:

```
.
├── dades.json
├── edicions-anteriors
│   └── ...
├── genera-web.py
├── index.html
├── llistes
│   ├── llista-1.pdf
│   ├── llista-2.pdf
│   ├── llista-3.pdf
│   └── llista-4.pdf
├── README.md
└── resolucions
    ├── llista-1
    │   ├── p1-1.pdf
    │   ├── p2-4.pdf
    │   ├── p3-13.pdf
    │   ├── p4-28.pdf
    │   ├── p5-58.pdf
    │   └── p6-31.pdf
    └── llista-2
        ├── p1-64.pdf
        ├── p2-130.pdf
        ├── p3-55.pdf
        ├── p4-112.pdf
        ├── p5-226.pdf
        └── p6-454.pdf
```

## Emblemes

En la edició del 2025 es van fer diversos canvis a la codebase per a donar suport a emblemes per als equips (o Polis). Per això, s'ha creat l'script auxiliar `convert-images.sh` que genera `.pdf` a partir de diversos formats d'imatge i s'ha afegit la carpeta de `img/equips`. És important que l'ordre en el que apareixen els equips en el `dades.json` sigui el mateix que l'ordre de les imatges, amb nom `grup-i.pdf` corresponent al grup amb índex `i` en `dades.json` (comencem indexant per 1).

## Edicions anteriors

Per tal de crear una edició nova (i suposant que l'última edició va ser el
2023), corre el script `genera-web.py` i copia els següents arxius:

- `public/llistes/2023` --> `edicions-anteriors/llistes/2023`
- `public/resolucions/2023` --> `edicions-anteriors/resolucions/2023`
- `public/index.html` --> `edicions-anteriors/2023.html`

Ara ja pots esborrar tots els fitxers de `llistes` i `resolucions` i
utilitzar-los per la nova edició (i canviar el fitxer `index.html` amb la nova
informació, no t'oblidis de posar un link a l'última edició al final de tot).
També s'han de canviar tots els llocs on apareix l'any a l'script `genera-web.py` i actualitzar els
terminis d'entrega a la funció `termini` d'aquest script.
